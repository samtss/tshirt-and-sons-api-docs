# Screen Print API

* Min order per batch is 50

## Option 1

### JSON organisation

* A _job_ is a unit of work for one piece of artwork. Different pieces of artwork should constitute multiple _jobs_.
* _prints_ define the artwork that is related to the _job_. There can be multiple _prints_ for a job, but each _print_ should be related to every _order_.
* an _order_ constitutes multiple _items_ destined for a single _recipient_.

### Jobs

| Name | Type | Required | Description |
| --- | --- | --- | --- | --- |
| job_ref | string | true | Your unique reference for this _job_ |
| deadline | date | false | When you'd like the job to be shipped by. Note that this is not a guaranteed shipment date, but something that can be used to help us allocate production runs |
| prints | array | true | an array of your _prints_ for this _job_. See _prints_ table. |
| orders | array | true | an array of your _orders_ for this _job_. see _orders_ table. |

### Prints

| Name | Type | Required | Description |
| --- | --- | --- | --- | --- |
| print_ref | string | true | Your unique reference for this _print_ ???? |
| type | string | false | What kind of _print_ is this? Default: `production` Options: `production`, `preview`|
| position | string | true | The location for the _print_, Options: `font`, `black`. |
| colors | string | true | The number of colours for the _print_. This should be reflected in the file provided in the _url_ field. |
| URL | string | true | The URL of the file to download. See _print specifications_ in order to find out the exact content this file should be. |
| name | string | false | The name of the file. |

### Orders

| Name | Type | Required | Description |
| --- | --- | --- | --- | --- |
| order_ref | string | true | Your unique reference for this _order_ |
| sale_datetime | datetime | true | the date/time of the sale |
| items | array | true | An array of the items that are part of this order. See _items_ |
| shipping | array | true | the recipient of this order. Note that currently only the **first** array item will be used |

### items

| Name | Type | Required | Description |
| --- | --- | --- | --- | --- |
| item_ref | string | true | your reference for this _item_ |
| quantity | integer | true | the number of this _item_ to be produced |
| variant_id | integer | true | What item should be used in production. This ID relates to a specific SKU |

### shipping

| Name | Type | Required | Description |
| --- | --- | --- | --- | --- |
| name | string | true |  The name of the _recipient_ of this _order_|
| address_1 | string | true |  First line of the address|
| address_2 | string | false |  Second line of the address|
| city | string | true |  Town or city |
| region | string | true | Usually a county or state |
| post_code | string | true | post or zip code |
| country_code | string | true | 2 digit ISO ??? Country code |
| method | string | false | The shipping method used to dispatch the item. Default: `standard`, options: `standard`, `express` |
| phone | string | false | recipient contact number. Normally it is recommended to include this in case of problems with delivery |
| email | string | false | recipient email |
| notes | string | false | Any delivery notes ? ? ? ? |


```json
{
    "job_ref":123,
    "deadline":"2014-08-11",
    "prints":[
        {
            "ref": "front_123_preview",
            "position":"front",
            "colors": 4,
            "url":"[complete image url]",
            "type": "preview"
        },
        {
            "ref": "front_123_print",
            "location":"front",
            "colors": 2,
            "url":"[complete image url]",
            "type": "preview"
        }
    ],
    "orders":[
        {    
            "order_ref": 12314,
            "sale_datetime": "",
            "shipping":[
            {
                "method":"STANDARD",
                "name":"James Dean",
                "address_1":"1234 Main Street",
                "address_2":"Suite 100",
                "address_3":"London",
                "address_4":"",
                "postcode":"EC1R 4TW",
                "country":"UNITED KINGDOM",
                "country_code":"GB",
                "phone":"+441373391645",
            },
            {
            "items":[
                {
                "item_ref":1237,
                "variant_id": "6400_black_small",
                "quantity":10
                }
            ]
            },
        ]
        }
    ]
}
```

## Option 2

### JSON organisation

* A _job_ is a unit of work made up of _orders_.
* _prints_ define the artwork that is related to the _order_. There can be multiple _prints_ for an _order_, but each _print_ will be applied to every _item_ in the _order_.
* an _order_ constitutes multiple _items_ destined for a single _recipient_.
* _orders_ will not go into production until the minimum requirements have been met (eg 50 of the same group of _prints_ )


## Option 3

### JSON organisation
* A _job_ is a unit of work made up of _orders_.
* _prints_ define the artwork that is related to the _item_. There can be multiple _prints_ for an _item_.
* An _order_ constitutes multiple _items_ destined for a single _recipient_.
* _orders_ will not go into production until the minimum requirements have been met for each _item_ (eg 50 of the same group of _prints_ ).
