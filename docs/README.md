# TShirt and Sons API Documetation

| Document | API Integration Guide |
| --- | --- |
| Version | 2.0 |
| Last Modified | 2016-01-27 |
| Contact Information | integrations@tshirtandsons.co.uk |

---

# Summary of the service

Tshirt and sons are a UK based company that provide print and distribution of garments

# Integration

TShirt and Sons provide a test API and a live fultulment API. Please arrange and carry out integration testing on the TEST API before changing your paramiters to use the LIVE API.

# Integration

## Test Endpoint settings

The test endpoint is used for all integration testing and is essentially identical to the live endpoint except orders will not get produced.

| Server | URL | Supported Methods |
| --- | --- | --- |
| api.tshirtandsons.co.uk/test_api/ | /v2/receive_order.php | HTTP/HTTPS |


## Live Endpoint Settings

| Server | URL | Supported Methods |
| --- | --- | --- |
| api.tshirtandsons.co.uk/live_api/ | /v2/receive_order.php | HTTP/HTTPS |

# Submitting Orders

In order to submit orders to the API, you will require and API token. API tokens are generated on a per customer basis and you will have to contact us to request one.

## Correct Syntax for data types

It is important to use the correct data types in the JSON data. The system will check for strings, integers and other data types. For example if you've submitted integer data such as "quantity" with quite marks around the value, it will be treated as a string and an error will be returned.

_this is INVALID formatting for the integer data type_
```json
{
    "quantity": "1"
}
```
_this is VALID formatting for the integer data type_
```json
{
    "quantity": 1
}
```
## Headers

| Name | Value | Description |
| --- | --- | --- |
| Authorization | _unique to customer_ | A unique alpha newmeric code identifying you as a legnitimate customer. This is a secret and should not be shared |

## Body

### Order

| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| order_id | integer | BigInt | 123323232 | This is _our_ unique identifier for your order, and should only be used if you're updating or canceling an order. If you're creating a new order this field should be excluded. If you do include it for a new order it will be ignored. |
| order_ref | string | varchar(255) | thinkshirt_123456 | A unique alpha newmeric code identifying your order for you. It must be unique across all your orders |
| order_response_url | string | varchar(255) | https://respnses.customer.com/order/thinkshirt_123456 | The response URL to use for this order, if different from standard URLS (used primarily for testing)|
| sale_datetime | date_time | YYYY-MM-DD HH:MM:SS |2015-12-22 01:15:20 | A date time of the sale |
| customer_name | string | varchar(255) | John Doe | the full name of the customer the order will be shipped to |
| shipping\_address_1 | string | varchar(255) | 64 Victoria Road | The first line of the shipping address, usually a house name or number|
| shipping\_address_2 | string | varchar(255) | Freshbrook | the second line of the address |
| shipping\_address_3 | string | varchar(255) | Springfield | the third line of the address, usually this is the town or city |
| shipping\_address_4 | string | varchar(255) | Wiltshire | the fourth line of the address, usually this is the county or region |
| shipping\_post_code | string | varchar(255) | SP3 2FD | the postal or zip code of the shipping address |
| shipping\_country | string | varchar(255) | Great Britain | the country for shipping (See Appendix for available shipping counties) |
| shipping\_country\_code | string | varchar(255) | GB | 2 digit iso code !LINK! |
| shipping_method | string | varchar(255) | - | Shipping method for this order |
| customer_phone | string | varchar(255) | +4401693123343 | The phone number for the customer |
| items | array | | | an array of order items |

### Order Items

| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| item_ref | string | varchar(255) | shirt_123123 | A unique reference for your order item |
| style | string | varchar(50) | mens | A style matching one of those available in the style guide |
| size | string | varchar(50) | medium | A size matching one of those available in the style guide |
| color | string | varchar(50) | blue | A colour matching one of those available in the style guide |
| quantity | integer | int(??) | 1 | The quantity required of this particular order item |
| prints | array | - | - | An array of print information for an order item |

### Order Item Prints

| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| print_location | string | varchar(50) | front | The location of the print on the garment, this should matching one of the available strings |
| print\_x_offset | integer | int(3) | 15 | The offset for the image to be printed on the x axis. This must be whole numbers and cannot be negitive values |
| print\_y_offset | integer | int(3) | 285 | The offset for the image to be printed on the y axis. This must be whole numbers and cannot be negitive values |
| print\_image_url | string | varchar(255) | http://media.example.com/images/01.jpg | the image URL for the fill resolution print image. See image format notes |
| preview\_image_url | string | varchar(255) | http://media.example.com/thumbnail/image1.jpg | the image URL the thumbnail preview image |


### Example JSON Requests

_An example for a single new order item, and single print location_
```json
{
    "order_ref":1234,
    "sale_datetime":"2014-08-11 01:15:20",
    "customer_name":"James Dean",
    "shipping_address_1":"1234 Main Street",
    "shipping_address_2":"Suite 100",
    "shipping_address_3":"London",
    "shipping_address_4":"",
    "shipping_postcode":"EC1R 4TW",
    "shipping_country":"UNITED KINGDOM",
    "shipping_country_code":"GB",
    "shipping_method":"STANDARD",
    "phone":"+441373391645",
    "items":[
        {
        "item_ref":1235,
        "style":"mens",
        "size":"medium",
        "color":"black",
        "quantity":1,
        "prints":[
            {
            "print_location":"front",
            "print_x_offset":15,
            "print_y_offset":285,
            "print_image_url":"[complete image url]",
            "preview_image_url":"[complete thumbnail url]"
            }
        ]
        }
    ]
}
```

_An example for a single new order with multiple items and multiple print locations_
```json
{
    "order_ref":1234,
    "sale_datetime":"2014-08-11 01:15:20",
    "customer_name":"James Dean",
    "shipping_address_1":"1234 Main Street",
    "shipping_address_2":"Suite 100",
    "shipping_address_3":"London",
    "shipping_address_4":"",
    "shipping_postcode":"EC1R 4TW",
    "shipping_country":"UNITED KINGDOM",
    "shipping_country_code":"GB",
    "shipping_method":"STANDARD",
    "phone":"+441373391645",
    "items":[
        {
        "item_ref":1235,
        "style":"mens",
        "size":"medium",
        "color":"black",
        "quantity":1,
        "prints":[
            {
            "print_location":"front",
            "print_x_offset":15,
            "print_y_offset":285,
            "print_image_url":"[complete image url]",
            "preview_image_url":"[complete thumbnail url]"
            },
            {
            "print_location":"back",
            "print_x_offset":0,
            "print_y_offset":0,
            "print_image_url":"[complete image url]",
            "preview_image_url":"[complete thumbnail url]"
            }
        ]
        },
        {
        "item_ref":1235,
        "style":"mens",
        "size":"medium",
        "color":"black",
        "quantity":1,
        "prints":[
            {
            "print_location":"front",
            "print_x_offset":105,
            "print_y_offset":200,
            "print_image_url":"[complete image url]",
            "preview_image_url":"[complete thumbnail url]"
            }
        ]
        }
    ]
}
```

## Order Submission Responses

Once an order is submitted you will receive a response from the API Gateway server. This response will tell you if your order was submitted to the gateway sucessfully. Please note that this does not neccisarily mean the submission was sucessfully accepted into production, it only means it's passed the first stage of our validation process.

| HTTP status code | Meaning | Message | Description |
| --- | --- | --- | --- |
| 202 | Accepted | | The order has been received by the API Gateway server, but processing has not been completed. You will receive an order_id as a response message, this should be kept by you incase you need to update orders on our system |
| 400 | Bad Request | | The request to the server is malformed, missing entries, not valid JSON or similar. Check the formatting and values you've submitted |
| 401 | Unauthorized | | The Authorization token used in the request has not been accepted, or was not included. Check the request headers and try again. |
| 500 | Server Error | | There has been a server error submitting your request to the system. If this happens you should contact us straight away with the details of the order, including the JSON submitted so we can investigate |

_Example response for a 202 accepted message. This provides a response status and an order id which will be unique to your order._
```json
{
    "order_id": 123123323232,
    "order_status": "Accepted"
}
```
Note: Order items are identified by your "item_ref" field, so you will not be provided additional ID information. You only need to store the order_id in order to fetch and update data from our system.

# Order Status Responses

Once an order has been submitted to the production server, you will begin to receive workflow updates. These updates are intended to provide you with an indication as to where the products in an order are in the production line.


| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| order_ref | string | varchar(255) | - | A unique alpha newmeric code identifying your order for you. It must be unique across all your orders |
| status | string | varchar(50) | received | The status of the order in production, this can be a set range of values |
| item | array | - | - | This array lists all items in the order that are affected by the status update or tracking code(s) |
| tracking | array | - | - | _OPTIONAL_ this array only appears when tracking numbers are available. For example, _EXPRESS_ shipping options for the UK will always have a tracking number, but _STANDARD_ shipping will not. If there's no tracking number this field will be omitted |

# Item

| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| item_ref | string | varchar(255) |  | A unique reference for your order item |

# Tracking

| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| number | string | varchar(255) |  | A unique tracking reference for your order |


# Artwork Specifications

All artwork submitted to the API must comply to these requirements:
- supplied as a PNG with a transparent background
- be the actual size for print
- a minimum resolution of 300DPI
- all fonts need to be converted to outline

## Print Dimensions

The maximum print dimentions are defined in the table below.

| Pallet | Width | Height |
| --- | --- | --- |
| Universal Pallet | 396mm | 490mm |
| Kids Pallet | 260mm | 320mm |
| Infants Pallet | 180mm | 180mm |

# Appendix

## Country List

This is a list of all acceptable countries for the shipping_country field:

https://docs.google.com/spreadsheets/d/1VkmjA95q29myruoedb_XgStC_iuTxWkXBtGvDXu8Y48/edit?usp=sharing

## Stock List

This is a multi-worksheet stock list for all items that can be provided by us:

https://docs.google.com/spreadsheets/d/1yuqmEfV0bQ9AAXDxJA3wRQm7cuqJLoryen2nsBsqy9I/edit?usp=sharing

## Shipping Methods

| Name | JSON string | Description |
| --- | --- | --- | --- |
| standard | STANDARD | standard shipping |
| express | EXPRESS | express shipping |

## Print locations

| Name | JSON string | Description |
| --- | --- | --- | --- |
| front | front | print for the front of the garment |
| back | back | print for the back of the garment |

## Server List

The following table indicates the servers that may interact with you over the course of the API request. This list is provided in the event that you require your firewalls to be configured to allow traffic from and to them.


| Server | FQDN | Type | Description |
| --- | --- | --- | --- |
| API Gateway | api.tshirtandsons.co.uk | HTTP, HTTPS | Incoming API requests will be submitted to this server, and initial responses sent back to the client |
| FileMaker | ? | HTTP | JSON status responses will be sent from this server as the order is sent through the production line |
