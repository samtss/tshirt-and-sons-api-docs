# TShirt and Sons API Documetation

| Document | API Integration Guide |
| --- | --- |
| Version | 2.0.2 |
| Last Modified | 2016-08-15 |
| Contact Information | integration@tshirtandsons.co.uk |

---


Tshirt and sons are a UK based company that provide digital print and distribution of garments worldwide. For more information on T Shirt and Sons please visit the website: http://tshirtandsons.co.uk/



T Shirt and Sons provide a test API and a live fulfillment API for digital print furfilment services. Please arrange and carry out integration testing on the test API before changing your parameters to use the live API. For details on how to connect to our API service, please contact us using the details on our website or at the top of this document.

# Integration

## Test Endpoint settings

The test endpoint is used for all integration testing and is essentially identical to the live endpoint except orders will not be produced.

| Server | URL | Supported Methods |
| --- | --- | --- |
| test.v2.api.tshirtandsons.co.uk/ | /v2/orders | HTTP |


## Live Endpoint Settings

| Server | URL | Supported Methods |
| --- | --- | --- |
| v2.api.tshirtandsons.co.uk/ | /v2/orders | HTTP |

# Submitting Orders

In order to submit orders to the API, you will require and API token (`api_token`). API tokens are generated on a per customer basis and you will have to contact us to request one.

## Correct Syntax for data types

It is important to use the correct data types in the JSON data. The system will check for strings, integers and other data types. For example if you've submitted integer data such as "quantity" with quite marks around the value, it will be treated as a string and an error will be returned.

_this is INVALID formatting for the integer data type_
```json
{
    "quantity": "1"
}
```
_this is VALID formatting for the integer data type_
```json
{
    "quantity": 1
}
```

## Available Methods

| METHOD | ROUTE | SERVER | PURPOSE
| -------- | -------- | ---- | ---- |
| POST | v2/orders | Test/Live | Create a new order


## JSON
### ORDERS

| Element | | Rules |
| -------- | -------- | -------- |
| api\_token | | Your unique API token. This can optionally be sent as a GET paramiter instead |
| test\_mode | | boolean |
| order\_ref | required | unique, alphanumeric including dashes or underscores |
| sale\_datetime | required | date format:Y/m/d H:i:s |
| recipient\_name | required | string |
| shipping\_address\_1 | required | string |
| shipping\_address\_2 | | string |
| shipping\_address\_3 | | string |
| shipping\_address\_4 | | string |
| shipping\_postcode | required | string |
| shipping\_country | required | string |
| shipping\_country\_code | required | string(2) |
| shipping\_method | required | in:STANDARD,EXPRESS |
| status\_update\_url | | accessible URL (used to issue single order status updates) |
| phone | | alpha numeric |
| items | required | array |

### ITEMS

| Element | | Rules |
| -------- | -------- | -------- |
| color | required |  alphanumeric including dashes or underscores |
| size | required |  alphanumeric including dashes or underscores |
| style | required |  alphanumeric including dashes or underscores |
| item\_ref | required |  unique, alphanumeric including dashes or underscores |
| quantity | required | integer |
| prints | required | array |

### PRINTS

| Element | | Rules |
| -------- | -------- | -------- |
| position | required | unique, in:front, back |
| print\_url | required | accessible url |
| preview\_url | required | accessible url |
| x\_offset | required | integer |
| y\_offset | required | integer |

```json
{
    "order_ref":2003,
    "sale_datetime":"2015/07/21 12:15:20",
    "recipient_name":"Your Name",
    "phone":"07470404227",
    "shipping_address_1":"123 Fake Road",
    "shipping_address_2":"Fake Street",
    "shipping_address_3":"Westbury",
    "shipping_postcode":"BA13 3DB",
    "shipping_country":"UNITED KINGDOM",
    "shipping_country_code":"GB",
    "shipping_method":"STANDARD",
    "items":[
      {
        "item_ref":20031,
        "style":"mens",
        "size":"large",
        "color":"white",
        "quantity":1,
        "prints":[
            {
                "position":"front",
                "x_offset":10,
                "y_offset":10,
                "print_url":"http://tshirtandsons.co.uk/api/test-garment-design.png",
                "preview_url":"http://tshirtandsons.co.uk/api/test-garment-mockup.jpg"
            }
        ]
      },
      {
        "item_ref":20032,
        "style":"mens",
        "size":"large",
        "color":"white",
        "quantity":1,
        "prints":[
            {
                "position":"back",
                "x_offset":10,
                "y_offset":10,
                "print_url":"http://tshirtandsons.co.uk/api/test-garment-design.png",
                "preview_url":"http://tshirtandsons.co.uk/api/test-garment-mockup.jpg"
            }
        ]
      }
   ]
}
```


## Order Submission Responses

| HTTP status code | Meaning  | Description |
| --- | --- | --- |
| 200 | Okay | The request has been completed successfully. Usually this is for deleting orders |
| 201 | Created | The order has been received by and created on the API server |
| 400 | Bad Request | The request to the server is malformed, missing entries, not valid JSON or similar. Check the formatting and values you've submitted |
| 409 | Conflict | The request to the server is causing a conflict of entries. You might be creating an entry that already exists. We've not processed this request. |
| 422 | Unprocessable Entity | The request to the server is missing entries or entries are in the wrong format. Check the formatting and values you've submitted |
| 401 | Unauthorized | The `api_token`used in the request has not been accepted, or was not included. Check the request headers and try again. |
| 403 | Forbidden | You're not allowed to access this resource with current `api_token` authentication level. |
| 404 | Not Found | You're trying to access a resource or URL that's not there |
| 500 | Server Error | There has been a server error submitting your request to the system. If this happens you should contact us straight away with the details of the order, including the JSON submitted so we can investigate |
| 501 | Not Implemented | Requested method is not implemented. |


### Order Status Responses

Once an order has been submitted to the production server, you will begin to receive workflow updates. These updates are intended to provide you with an indication as to where the products in an order are in the production line.


| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| id | string | varchar(255) | - | A unique alpha numeric code identifying your order for you. It must be unique across all your orders |
| status | string | varchar(50) | received | The status of the order in production, this can be a set range of values |
| item | array | - | - | This array lists all items in the order that are affected by the status update or tracking code(s) |
| tracking | array | - | - | _OPTIONAL_ this array only appears when tracking numbers are available. For example, _EXPRESS_ shipping options for the UK will always have a tracking number, but _STANDARD_ shipping will not. If there's no tracking number this field will be omitted |

#### Item

| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| id | string | varchar(255) |  | A unique reference for your order item |

####Tracking

| Name | Type | Format | Example | Description |
| --- | --- | --- | --- | --- |
| number | string | varchar(255) |  | A unique tracking reference for your order |

### Example of status response message

```json
{
    "order": {
        "id": 259237,
        "status": "received",
        "item": [
            {
                "id": 13792
            },
            {
                "id": 13792
            }
        ]
    }
}
```

# Artwork Specifications

All artwork submitted to the API must comply to these requirements:
- supplied as a PNG with a transparent background
- be the actual size for print
- a minimum resolution of 300DPI
- all fonts need to be converted to outline

## Print Dimensions

The maximum print dimensions are defined in the table below.

| Pallet | Width | Height |
| --- | --- | --- |
| Universal Pallet | 396mm | 490mm |
| Kids Pallet | 260mm | 320mm |
| Infants Pallet | 180mm | 180mm |

# Appendix

## Country List

This is a list of all acceptable countries for the shipping_country field:

https://docs.google.com/spreadsheets/d/1VkmjA95q29myruoedb_XgStC_iuTxWkXBtGvDXu8Y48/edit?usp=sharing

## Stock List

This is a multi-worksheet stock list for all items that can be provided by us:

https://docs.google.com/spreadsheets/d/1yuqmEfV0bQ9AAXDxJA3wRQm7cuqJLoryen2nsBsqy9I/edit?usp=sharing

## Shipping Methods

| Name | JSON string | Description |
| --- | --- | --- |
| standard | STANDARD | standard shipping |
| express | EXPRESS | express shipping |

## Print locations

| Name | JSON string | Description |
| --- | --- | --- |
| front | front | print for the front of the garment |
| back | back | print for the back of the garment |
